package com.projektgrupowy.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PasswordValidatorTest {

	@InjectMocks
	private PasswordValidator passwordValidator;

	@Test
	public void testValidPasswords() {
		String[] passwords = {"Testdc88", "Teestdc88", "Teestdc!@", "pAssword0"};

		for(String password : passwords) {
			Assert.assertTrue("Failed at: " + password, passwordValidator.validatePassword(password));
		}
	}

	@Test
	public void testInvalidPasswords() {
		String[] passwords = {
				"testdc", // obviously too weak password
				"Teeestdc88", // more than 2 same characters in a row
				"Teestddc", // No digit or special character
				"testdc88", // No uppercase letter
				"123456", "qwerty", "admin1",
				null // yeah, good luck with that
		};

		for(String password : passwords) {
			Assert.assertFalse(passwordValidator.validatePassword(password));
		}
	}
}
