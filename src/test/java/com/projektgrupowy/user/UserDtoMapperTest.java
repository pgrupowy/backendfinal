package com.projektgrupowy.user;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

/**
 * HERE BE DRAGONS
 * DO NOT RUN!
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDtoMapperTest {

	@InjectMocks
	private UserDtoMapper userDtoMapper;

	@Test
	public void validateMapperDate() {
		User u = User.builder().birthDate(LocalDate.of(2000, 10, 1))
				.firstName("Test")
				.lastName("Test")
				.build();

		UserDto dto = userDtoMapper.mapToDto(u);

		Assert.assertEquals(17, (long)dto.getAge());

	}
}
