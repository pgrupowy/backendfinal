package com.projektgrupowy.track;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ParsedTrackDao extends JpaRepository<ParsedTrack, Integer> {

	@Query(value = "SELECT pt FROM ParsedTrack pt WHERE pt.owner.id = :userId")
	List<ParsedTrack> findAllByOwnerId(@Param("userId") Integer userId);

	@Query(value = "SELECT track FROM ParsedTrack AS track WHERE (track.id<>:trackId) AND ((:trackEndDate BETWEEN track.startDate AND track.endDate) OR (track.endDate BETWEEN :trackStartDate AND :trackEndDate))")
	List<ParsedTrack> findTracksOverlappingWithTrack(@Param("trackId") Integer trackId,
													 @Param("trackStartDate") LocalDateTime trackStartDate,
													 @Param("trackEndDate") LocalDateTime trackEndDate);
	//List<ParsedTrack> findTracksOverlappingWithTrack(@Param("trackId") Integer trackId);
	// @Query(value = "SELECT track FROM ParsedTrack AS track WHERE track.id<>:trackId")
}
