package com.projektgrupowy.track;

import org.springframework.stereotype.Service;

@Service
class ParsedTrackDtoMapper {

	ParsedTrackDto mapToDto(ParsedTrack track) {

		return ParsedTrackDto.builder()
							 .id(track.getId())
							 .name(track.getName())
							 .startDate(track.getStartDate())
							 .endDate(track.getEndDate())
							 .build();
	}

}
