package com.projektgrupowy.track;

import com.projektgrupowy.track.comparator.TrackComparedData;
import com.projektgrupowy.user.User;
import com.projektgrupowy.utils.FileUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Date;
import java.util.HashMap;

@Service
public class TrackStorageService {

	private static final String CACHE_FILE_EXTENSION = ".comparedCache";

	public Serializable getFromCache(ParsedTrack track, Class clazz) {
		Serializable out;

		if(clazz.equals(ParsedTrack.class)) {
			out = retrieveFromCache(track.getFilePath());
		} else if(clazz.equals(TrackComparedData.class)) {
			out = retrieveFromCache(track.getFilePath() + CACHE_FILE_EXTENSION);
			if(out == null) {
				out = TrackComparedData.builder()
									   .trackId(track.getId())
									   .trackPath(track.getFilePath())
									   .cachedEntries(new HashMap<>())
									   .build();
			}
		} else {
		 	throw new UnsupportedOperationException("This is not cacheable class!");
		}

		return out;
	}

	private Serializable retrieveFromCache(String filePath) {
		try (FileInputStream fileIn = new FileInputStream(filePath);
			 ObjectInputStream in = new ObjectInputStream(fileIn)){

			Object file = in.readObject(); // powiedzieć dziewczynom o środku pleśniobójczym na szafce
			in.close();
			fileIn.close();
			return (Serializable) file;

		} catch (IOException i) {
			return null;
		} catch (ClassNotFoundException c) {
			System.out.println("Track class not found");
			c.printStackTrace();
			return null;
		}
	}

	String cacheParsedTrack(Serializable object, User user) {
		String path = prepareTrackFilePath(user);

		FileUtils.createConditionallyDirectoryForFile(path);
		FileUtils.saveObjectToFile(object, path);

		return path;
	}

	public void cacheParsedSegments(ParsedTrack track, TrackComparedData data) {
		String path = track.getFilePath() + CACHE_FILE_EXTENSION;
		FileUtils.saveObjectToFile(data, path);
	}

	private String prepareTrackFilePath(User user) {
		return String.join(File.separator,
				user.getId().toString(),
				"tracks",
				String.valueOf(new Date().getTime())) + ".obj";
	}
}
