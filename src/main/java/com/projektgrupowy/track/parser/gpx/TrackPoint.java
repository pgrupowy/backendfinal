package com.projektgrupowy.track.parser.gpx;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class TrackPoint implements Serializable {

	@JacksonXmlProperty(localName = "lat")
	BigDecimal latitude;

	@JacksonXmlProperty(localName = "lon")
	BigDecimal longitude;

	LocalDateTime time;

	@JacksonXmlProperty(localName = "ele")
	Double elevation;

}
