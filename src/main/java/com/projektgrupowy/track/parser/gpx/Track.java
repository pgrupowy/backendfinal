package com.projektgrupowy.track.parser.gpx;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class Track implements Serializable {

	String name;

	@JacksonXmlProperty(localName = "trkseg")
	TrackSegment segment;

}

