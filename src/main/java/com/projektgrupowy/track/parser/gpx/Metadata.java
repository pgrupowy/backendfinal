package com.projektgrupowy.track.parser.gpx;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class Metadata implements Serializable {

	LocalDateTime time;

}