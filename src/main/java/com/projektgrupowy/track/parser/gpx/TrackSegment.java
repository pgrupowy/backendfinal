package com.projektgrupowy.track.parser.gpx;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TrackSegment implements Serializable {

	@JacksonXmlProperty(localName = "trkpt")
	List<TrackPoint> points;

}
