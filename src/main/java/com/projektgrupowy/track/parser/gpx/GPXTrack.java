package com.projektgrupowy.track.parser.gpx;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JacksonXmlRootElement
public class GPXTrack implements Serializable {

	@JacksonXmlProperty(localName = "metadata")
	Metadata meta;

	@JacksonXmlProperty(localName = "trk")
	Track track;

	String creator;
	String version;
	String schemaLocation;
}
