package com.projektgrupowy.track;

import com.projektgrupowy.track.comparator.TracksProcessor;
import com.projektgrupowy.track.parser.GPXParser;
import com.projektgrupowy.track.parser.gpx.GPXTrack;
import com.projektgrupowy.track.parser.gpx.TrackPoint;
import com.projektgrupowy.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rest/tracks")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TrackController {

	private static final Logger LOG = LoggerFactory.getLogger(TrackController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private GPXParser parser;
	@Autowired
	private ParsedTrackDao parsedTrackDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ParsedTrackDtoMapper parsedTrackDtoMapper;
	@Autowired
	private TrackStorageService trackStorageService;
	@Autowired
	private TracksProcessor tracksProcessor;
	@Autowired
	private UserDtoMapper userDtoMapper;

	@PostMapping(value = "")
	public GPXTrack uploadGpxTrack(@RequestParam("file") MultipartFile multipartFile, Principal principal) throws IOException {
		User loggedUser = userService.getUserByPrincipal(principal);
		GPXTrack track = parser.parseToTrack(multipartFile);
		String trackPath = trackStorageService.cacheParsedTrack(track, loggedUser);

		LOG.info("User {} uploaded track of name {}", loggedUser.getId(), track.getTrack().getName());

		List<TrackPoint> trackPoints = track.getTrack().getSegment().getPoints();

		ParsedTrack trackEntity = ParsedTrack.builder()
											 .filePath(trackPath)
											 .owner(loggedUser)
											 .name(track.getTrack().getName())
											 .startDate(trackPoints.get(0).getTime())
											 .endDate(trackPoints.get(trackPoints.size()-1).getTime())
											 .uploadDate(LocalDateTime.now())
											 .build();

		parsedTrackDao.saveAndFlush(trackEntity);
		loggedUser.addTrack(trackEntity);
		userDao.saveAndFlush(loggedUser);
		return track;
	}

	@GetMapping("")
	public List<ParsedTrackDto> getListOfTracksByLoggedUser(Principal principal) {
		User user = userService.getUserByPrincipal(principal);

		return user.getRegisteredTracks().stream()
				   .map(parsedTrackDtoMapper::mapToDto)
				   .collect(Collectors.toList());
	}

	@GetMapping("{id}")
	public ParsedTrackDto getTrackById(@PathVariable Integer id, Principal principal) throws Exception {
		User loggedUser = userService.getUserByPrincipal(principal);
		ParsedTrack track = parsedTrackDao.findOne(id);
		validateTrackAccess(loggedUser, track);

		LOG.info("User {} requested track #{}", loggedUser.getId(), id);

		GPXTrack gpxTrack = (GPXTrack) trackStorageService.getFromCache(track, ParsedTrack.class);
		ParsedTrackDto dto = parsedTrackDtoMapper.mapToDto(track);
		dto.setTrackPoints(gpxTrack.getTrack().getSegment().getPoints());

		return dto;
	}

	@GetMapping("{id}/user")
	public UserDto getUserByTrackId(@PathVariable Integer id, Principal principal) throws Exception {
		User loggedUser = userService.getUserByPrincipal(principal);

		LOG.info("User {} requested profile of track's owner by track id #{}", loggedUser.getId(), id);

		ParsedTrack track = parsedTrackDao.findOne(id);
		return userDtoMapper.mapToDto(track.getOwner());
	}

	@GetMapping("{id}/relatives")
	public Map<Integer, List<TrackPoint>> getRelativesMap(@PathVariable Integer id, Principal principal) throws Exception {
		User loggedUser = userService.getUserByPrincipal(principal);

		LOG.info("User {} requested relatives to track #{}", loggedUser.getId(), id);

		return tracksProcessor.getListOfCommonPointsFor(id, loggedUser);
	}

	private void validateTrackAccess(User user, ParsedTrack track) throws Exception {
		if(!track.getOwner().getId().equals(user.getId())) {
			throw new Exception("Access Violation");
		}
	}
}
