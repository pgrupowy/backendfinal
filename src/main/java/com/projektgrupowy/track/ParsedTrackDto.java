package com.projektgrupowy.track;

import com.projektgrupowy.track.parser.gpx.TrackPoint;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
class ParsedTrackDto {

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private String name;
	private Integer id;
	private List<TrackPoint> trackPoints;

}
