package com.projektgrupowy.track.comparator;

import com.projektgrupowy.track.ParsedTrack;
import com.projektgrupowy.track.ParsedTrackDao;
import com.projektgrupowy.track.TrackStorageService;
import com.projektgrupowy.track.parser.gpx.GPXTrack;
import com.projektgrupowy.track.parser.gpx.TrackPoint;
import com.projektgrupowy.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.AccessException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class TracksProcessor {

	private static final long MAX_TIME_DIFFERENCE_IN_SECONDS = 90_000L; // About 90s.

	private static final Logger LOG = LoggerFactory.getLogger(TracksProcessor.class);

	@Autowired
	private ParsedTrackDao parsedTrackDao;
	@Autowired
	private TrackStorageService trackStorageService;

	public Map<Integer, List<TrackPoint>> getListOfCommonPointsFor(Integer trackId, User user) throws AccessException {
		ParsedTrack track = parsedTrackDao.findOne(trackId);
		validateTrackAccess(track, user);
		TrackComparedData data;
//		try {
//			data = processTrack(track).get(1000, TimeUnit.SECONDS);
//		} catch (InterruptedException | ExecutionException | TimeoutException e) {
//			data = (TrackComparedData)trackStorageService.getFromCache(track, TrackComparedData.class);
//		}
		data = (TrackComparedData)trackStorageService.getFromCache(track, TrackComparedData.class);

		return data.getCachedEntries();
	}

	@Scheduled(fixedRate = 2*60*1000L)
	void delegateTracksProcessing() {
		LOG.info("Started tracks processing");
		List<ParsedTrack> allTracks = parsedTrackDao.findAll();

		for(ParsedTrack track : allTracks) {
			processTrack(track);
		}

		LOG.info("Tracks processing ended");
	}

	@Async
	private CompletableFuture<TrackComparedData> processTrack(ParsedTrack track) {
		GPXTrack gpxTrack = (GPXTrack) trackStorageService.getFromCache(track, ParsedTrack.class);
		TrackComparedData trackCache = (TrackComparedData) trackStorageService.getFromCache(track, TrackComparedData.class);

		List<ParsedTrack> interlappingTracks = parsedTrackDao.findTracksOverlappingWithTrack(track.getId(), track.getStartDate(), track.getEndDate());

		if(trackCache.getCachedEntries().size() < interlappingTracks.size()) {
			for (ParsedTrack interlappingTrack : interlappingTracks) {
				if(trackCache.getCachedEntries().containsKey(interlappingTrack.getId())) {
					continue;
				}

				GPXTrack interlappingGpxTrack = (GPXTrack) trackStorageService.getFromCache(interlappingTrack, ParsedTrack.class);
				List<TrackPoint> matched = new ArrayList<>();
				for (TrackPoint point : gpxTrack.getTrack().getSegment().getPoints()) {
					for (TrackPoint interlappingPoint : interlappingGpxTrack.getTrack().getSegment().getPoints()) {
						if (pointsConsideredCommon(point, interlappingPoint)) {
							matched.add(point);
						}
					}
				}
				trackCache.getCachedEntries().put(interlappingTrack.getId(), matched);
			}
		}

		trackStorageService.cacheParsedSegments(track, trackCache);

		return CompletableFuture.completedFuture(trackCache);
	}

	private void validateTrackAccess(ParsedTrack track, User user) throws AccessException {
		if(!track.getOwner().getId().equals(user.getId())) {
			throw new AccessException("User has no access to this track");
		}
	}
	private boolean pointsConsideredCommon(TrackPoint main, TrackPoint tested) {

		return Duration.between(main.getTime(), tested.getTime()).toMillis() <= MAX_TIME_DIFFERENCE_IN_SECONDS
		    && getDistanceInMeters(main, tested) <= 60;

	}

	private static double getDistanceInMeters(TrackPoint first, TrackPoint second) {
		BigDecimal theta = first.getLongitude().subtract(second.getLongitude());
		double firstLatInRadians = deg2rad(first.getLatitude()).doubleValue();
		double secondLatInRadians = deg2rad(second.getLatitude()).doubleValue();

		double dist = Math.sin(firstLatInRadians) * Math.sin(secondLatInRadians)
					+ Math.cos(firstLatInRadians) * Math.cos(secondLatInRadians) * Math.cos(deg2rad(theta).doubleValue());

		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1853.15962;

		return (dist);
	}

	private static BigDecimal deg2rad(BigDecimal deg) {
		return deg.multiply(new BigDecimal("3.14159265358979323846")).divide(BigDecimal.valueOf(180.0), BigDecimal.ROUND_HALF_UP);
	}

	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
}
