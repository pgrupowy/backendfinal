package com.projektgrupowy.track.comparator;

import com.projektgrupowy.track.parser.gpx.TrackPoint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@AllArgsConstructor
@Builder
public class TrackComparedData implements Serializable {

	private Integer trackId;
	private String trackPath;
	private Map<Integer, List<TrackPoint>> cachedEntries;

}
