package com.projektgrupowy.track.comparator;

import com.projektgrupowy.track.parser.gpx.TrackPoint;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
class TrackComparedCacheEntry {

	List<TrackPoint> matchedTrackpoints;
}
