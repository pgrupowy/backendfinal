package com.projektgrupowy.security;

import com.projektgrupowy.user.UserDto;
import com.projektgrupowy.user.UserDtoMapper;
import com.projektgrupowy.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/rest/login")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private UserDtoMapper userDtoMapper;

	@GetMapping("")
	public UserDto login(Principal principal) {
		User userAuth = (User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
		LOG.info("Logging user {}", userAuth.getUsername());

		return userDtoMapper.mapToDto(userService.getUserByPrincipal(principal));
	}
}
