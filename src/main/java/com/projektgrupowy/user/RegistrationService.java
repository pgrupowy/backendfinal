package com.projektgrupowy.user;

import com.projektgrupowy.utils.PasswordValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;

@Service
public class RegistrationService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private PasswordValidator passwordValidator;
	@Autowired
	private PasswordEncoder passwordEncoder;

	private static final Logger LOG = LoggerFactory.getLogger(RegistrationService.class);
	/**
	 * Should return ID of registered user, or throw and exception when it is impossible to create an user
	 * @param registrationDto
	 * @return
	 */
	Integer registerUser(UserRegistrationDto registrationDto) {
		validateRegistrationDto(registrationDto);

		User u = User.builder()
					 .firstName(registrationDto.getFirstName())
					 .lastName(registrationDto.getLastName())
					 .username(registrationDto.getUsername())
					 .birthDate(registrationDto.getBirthDate())
					 .email(registrationDto.getEmail())
					 .password(passwordEncoder.encode(registrationDto.getPassword()))
					 .role("ROLE_USER")
					 .enabled(true)
					 .build();

		userDao.save(u);

		LOG.debug("Registered user: {}", u);

		return u.getId();
	}

	private void validateRegistrationDto(UserRegistrationDto registrationDto) throws ValidationException {
		if( ! registrationDto.getPassword().equals(registrationDto.getPasswordRepeated())) {
			throw new ValidationException("Passwords do not match");
		}

		if(userDao.findOneByUsername(registrationDto.getUsername()).isPresent()) {
			throw new ValidationException("Username already taken");
		}

		if( ! passwordValidator.validatePassword(registrationDto.getPassword())) {
			throw new ValidationException("Invalid password");
		}
	}
}
