package com.projektgrupowy.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	public User getUserByPrincipal(Principal principal) {
		return userDao.findOneByUsername(principal.getName()).orElse(null);
	}
}
