package com.projektgrupowy.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {

	private String fullName;
	private String username;
	private Integer age;
	private String city;
	private String sex;
	private String avatar;

}
