package com.projektgrupowy.user;

import com.projektgrupowy.track.ParsedTrack;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {

	public static final DateTimeFormatter BIRTH_DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	public static final String SEX_FEMALE = "female";
	public static final String SEX_MALE = "male";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "city")
	private String city;

	@Column(name = "avatar")
	private String avatar;

	@Column(name = "sex")
	private String sex;

	@Column(name = "birth_date")
	private LocalDate birthDate;

	@Column(name = "email")
	private String email;

	@Column(name = "role")
	private String role;

	@Column(name = "enabled")
	private boolean enabled;

	// TODO: Friends
	//	private List<User> friends;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ParsedTrack> registeredTracks;

	String getFullName() {
		return String.format("%s %s", firstName, lastName);
	}

	public User addTrack(ParsedTrack t) {
		if(registeredTracks == null) {
			this.registeredTracks = new ArrayList<>();
		}

		this.registeredTracks.add(t);
		return this;
	}
}
