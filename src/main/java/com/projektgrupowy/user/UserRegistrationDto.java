package com.projektgrupowy.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.projektgrupowy.utils.BirthDateDeserializer;
import com.projektgrupowy.utils.BirthDateSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
class UserRegistrationDto {

	private String username;
	private String firstName;
	private String lastName;
	private String password;
	private String passwordRepeated;
	private String email;

	@JsonSerialize(using = BirthDateSerializer.class)
	@JsonDeserialize(using = BirthDateDeserializer.class)
	private LocalDate birthDate;

}
