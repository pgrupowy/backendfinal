package com.projektgrupowy.user;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;

@Service
public class UserDtoMapper {

	public UserDto mapToDto(User u) {
		return UserDto.builder()
					  .age(Period.between(u.getBirthDate(), LocalDate.now()).getYears())
					  .city(u.getCity())
					  .sex(u.getSex())
					  .avatar(u.getAvatar())
					  .fullName(u.getFullName())
					  .build();
	}
}
