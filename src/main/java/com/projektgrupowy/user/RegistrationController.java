package com.projektgrupowy.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("registration")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RegistrationController {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	private RegistrationService registrationService;

	@PostMapping("user")
	public boolean registerUser(@RequestBody @Valid UserRegistrationDto registrationDto) {
		Integer userId = registrationService.registerUser(registrationDto);

		LOG.debug("Registered user of ID: {}", userId);

		return true;
	}
}
