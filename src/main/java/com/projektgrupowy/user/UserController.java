package com.projektgrupowy.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/users/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	@Autowired
	private UserDtoMapper userDtoMapper;
	@Autowired
	private UserDao userDao;

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	@GetMapping("{id}")
	public UserDto getUserDto(@PathVariable final String id) {
		return userDtoMapper.mapToDto(userDao.findOne(Integer.valueOf(id)));
	}
}
