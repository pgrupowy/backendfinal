package com.projektgrupowy.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class PasswordValidator {

	private static final Logger LOG = LoggerFactory.getLogger(PasswordValidator.class);

	/*
	 * 8-32 characters
	 * No more than 2 equal characters in a row
	 * At least one uppercase
	 * At least one special character or digit
	 *
	 */
	private final static Pattern PASSWORD_PATTERN = Pattern.compile("^(?:(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))(?!.*(.)\\1{2,})[A-Za-z0-9!~<>,;:_=?*+#.\"&§%°()\\|\\[\\]\\-\\$\\^\\@\\/]{8,32}$");

	public boolean validatePassword(String password) {

		if(null == password) {
			LOG.warn("Null password passed for validation!");
			return false;
		}

		return PASSWORD_PATTERN.matcher(password).matches();
	}
}
