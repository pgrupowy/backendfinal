package com.projektgrupowy.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.projektgrupowy.user.User;

import java.io.IOException;
import java.time.LocalDate;

public class BirthDateSerializer extends StdSerializer<LocalDate> {

	public BirthDateSerializer() {
		super(LocalDate.class);
	}

	@Override
	public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		jsonGenerator.writeString(localDate.format(User.BIRTH_DATE_FORMAT));
	}
}
