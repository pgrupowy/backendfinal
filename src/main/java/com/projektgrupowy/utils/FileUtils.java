package com.projektgrupowy.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {

	public static boolean createConditionallyDirectoryForFile(String filePath) {
		try {
			Files.createDirectories(Paths.get(filePath.substring(0, filePath.lastIndexOf(File.separator))));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean saveObjectToFile(Serializable object, String path) {
		try {
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(object);
			out.close();
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
