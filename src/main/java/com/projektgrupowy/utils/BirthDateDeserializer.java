package com.projektgrupowy.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.projektgrupowy.user.User;

import java.io.IOException;
import java.time.LocalDate;

public class BirthDateDeserializer extends StdDeserializer<LocalDate> {

	public BirthDateDeserializer() {
		super(LocalDate.class);
	}

	@Override
	public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		return LocalDate.parse(jsonParser.readValueAs(String.class), User.BIRTH_DATE_FORMAT);
	}
}
